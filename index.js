// Operators

// Assignment Operators
// Basic assignment operator (=)
let assignmentNumber = 8;

//Arithmetic Assignment operators 
// Addition assignment operators (+=)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction assignment operator (-=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

// multiplication assignment operator (*=)
assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

// division assignment operator (*=)
assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

// Arithmetic operators

let x = 1397;
let y = 7831;

let sum = x + y; 
console.log("Result of addition operator: " + sum);

let difference = x - y; 
console.log("Result of addition operator: " + difference);

let product = x * y; 
console.log("Result of addition operator: " + product);

let quotient = x / y; 
console.log("Result of addition operator: " + quotient);

let remainder = x % y;
console.log("Result of remainder operator: " + remainder);

// Multiple Operators and Parentheses
// using mdas
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas); 

// using parentheses
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of mdas operation: " + pemdas);

// Increment (++) and Decrement (--)
let z = 1;
// Pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// Psot-increment
increment = z++;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// Pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Post-decrement
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);


// Type coercion

let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = true + 1;
console.log(numC);

let numD = false + 1;
console.log(numD);

// Relational operators

// Equality operator (==)

let juan = 'juan'; 
console.log(1 == 1); 
console.log(1 == 2);
console.log(1 == "1");
console.log("juan" == juan);

// Inequality operator(!=)
console.log("Less than and greater than");
console.log(1 != 1); 
console.log(1 != 2);
console.log(1 != "1");
console.log("juan" != juan);

// <, >, <=, >=
console.log(4 < 6);
console.log(2 > 8);
console.log(5 >= 5);
console.log(10 <= 15);

// Strict Equality operator (====)
console.log("Strict equality:");
console.log(1 === '1');

// Strict innequality operator (!===)
console.log("Strict innequality:");
console.log(1 !== '1');


// Logical operators
let isLegalAge = true;
let isRegistered = false;

// And Operator (&&)
// Returns true if all operrands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical And operator: " + allRequirementsMet);

// OR Operator (||)
// Return true if one of the operands is true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Selection control structure

// if, else if and else statement

// if statement
// Executes a statement if a specified condition is true
/* Syntax
	if(condition){
		statement/s;
	}
*/

let numE = -1; 

if (numE < 0){
	console.log("Hello");
}


let nickName = "Matheo";

if(nickName === "Matt"){
	console.log("Hello " + nickName);
}

// else if clause 
// executes a statement if previous conditions are false and if the specified conditions is true

/*
	if(condition1){
		statement/s;
	}

	else if(condition2){
		statement/s;
	}
	}

	else if(condition3){
		statement/s;
	}
*/

let numF = 1; 

if(numE > 0){
	console.log("Hello");
}
else if(numF > 0){
	console.log("World");
}

// else clause 

/*
	if(condition1){
		statement/s;
	}

	else if(condition2){
		statement/s;
	}

	else if(condition3){
		statement/s;
	}
	else if(conditionN){
		statement/s;
	}
*/

/*
	numE = -1;
	numF = 1;
*/

if(numE > 0){
	console.log("Hello");
}
else if(numF === 0){
	console.log("World");
}
else{
	console.log("Again");
}


let message = 'No messaege';
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return 'Not a typhoon yet.';
	}
	else if(windSpeed <= 60){
		return 'Topical depression detected';
	}
	else if (windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected';
	}
	else if (windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected';
	}
	else{
		return 'Typhoon detected';
	}

}

message = determineTyphoonIntensity(68);
console.log(message);

let department = prompt("Enter your department");

if(department === 'a' || decrement === 'A'){
	console.log("You are in department A");
}
if(department === 'b' || decrement === 'B'){
	console.log("You are in department B");
}


// Ternary operator 
/* Syntax
	(expression) ? ifTrue : ifFalse;
*/

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit';
}

function isOfunderAge(){
	name = 'Jane';
	return 'You are under the age limit';
}

// ParseInt converts string values into numeric types
let age = parseInt(prompt('What is your age?'));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isOfunderAge();
console.log('Result of Ternary Operator in function: ' + legalAge + ", " + name);

// Switch Statement
/* Syntax
	switch (expression){
		case value1:
			statement;
			break;
		case value2:
			statement;
			break;
		case value3:
			statement;
			break;
		case valuen:
			statement;
			break;
		default:
			statement;
	}
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day){
	case 'sunday':
		console.log("The color of the day is red");
		break;
	case 'monday':
		console.log("The color of the day is orange");
		break;
	case 'tuesday':
		console.log("The color of the day is yellow");
		break;
	case 'wednesday':
		console.log("The color of the day is green");
		break;
	case 'thursday':
		console.log("The color of the day is blue");
		break;
	case 'friday':
		console.log("The color of the day is indigo");
		break;
	case 'saturday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");
}

// Try-Catch-finally Statement

function showIntensityAlert(windSpeed){
	try{
		// Attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	}
	// error/err are commonly used variable by developers for storing errors
	catch (error){
		console.log(typeof error);

		console.warn(error.message);
		// error.message is used to accesss the information relating to an error object
	}
	finally{
		// Continue on executing the code regardless of successs or failure of code execution the 'try' block.
		alert('Intensity updates will show new alert.');
	}
}

showIntensityAlert(56);
